package org.StudentDBApp;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Ask how many number of students in database
        System.out.println("How many students to be enrolled? ");
        Scanner in = new Scanner(System.in);
        int numOfStudents = in.nextInt();
        Student[] students = new Student[numOfStudents];

        // Create number of new students
        for (int n = 0; n < numOfStudents; n++){
            students[n] = new Student();
            Student student = students[n];
            student.enroll();
            student.payTuition();
            System.out.println(student.showInfo());
        }
        // System.out.println("Hello world!");
    }

}