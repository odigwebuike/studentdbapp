package org.StudentDBApp;

import java.util.Scanner;

public class Student {

    private String firstName;
    private String lastName;
    private int gradeYear;

    private String studentID;
    private String courses = "";
    private int tuitionBalance = 0;
    private int costOfCourse = 600;

    private static int id = 0001;


    // Constructor prompts user to add new student's name and year
    public Student() {

        System.out.println("Enter student's first name: ");
        Scanner in = new Scanner(System.in);

        this.firstName = in.nextLine();

        System.out.println("Enter student's last name: ");
        this.lastName = in.nextLine();

        System.out.println("Enter student's class level (100 for 1st year, 200 for 2nd year, 300 for 3rd year or 400 for final year): ");
        this.gradeYear = in.nextInt();
        setStudentID();

    }
    // Generate an ID
    private void setStudentID() {

        id++;
        this.studentID = gradeYear + "" + id;

    }
    // Enrol in courses
    public void enroll() {

       do  {
            System.out.println("Enter course to enroll(Q to quit): ");
            Scanner in = new Scanner(System.in);
            String course = in.nextLine();
            // if (course != "Q" || course != "q") {
           if (!course.equals("Q")) {
                    courses = courses + "\n" + course;
                    tuitionBalance = tuitionBalance + costOfCourse;
                    // System.exit(0);
                } else {
                    System.out.println("BREAK!!");
                    break;
                }
        }
       while (true);

    }
    // View balance and pay tuition
    public void viewBalance() {
        System.out.println("Your tuition balance is: $" + tuitionBalance);
    }
    // Show status
    public void payTuition() {
        viewBalance();
        System.out.println("Enter your tuition payment amount: $");
        Scanner in =  new Scanner(System.in);
        int payment = in.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank you for your tuition payment of $" + payment);
        System.out.println("Your tuition balance is now: $" + tuitionBalance);
    }

    public String showInfo() {
        return "Student ID: " + studentID + "\n" +
                "Name: " + firstName + " " + lastName + "\n" +
                "Grade Level: " + gradeYear + "\n" +
                "Courses Enrolled: " + courses + "\n" +
                "Tuition Balance: $" + tuitionBalance + "\n";
    }

}
